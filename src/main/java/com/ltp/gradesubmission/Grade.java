package com.ltp.gradesubmission;

import java.util.UUID;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Grade {
    private String name;
    private String subject;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date score;
    private String id;

    public Grade() {
        this.id = UUID.randomUUID().toString();
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getScore() {
        return score;
    }

    public void setScore(Date score) {
        this.score = score;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getId() {
        return id;
    }
}
