package com.ltp.gradesubmission;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GradeController {

    List<Grade> grades = new ArrayList<>();

    @GetMapping("/grades")
    public String getGrade(Model model){
        model.addAttribute("grades", grades);
        return "grades";
    }

    @GetMapping("/")
    public String getForm(Model model, @RequestParam(required = false) String id){
        Grade grade;
        int index = getGradeIndex(id);
        model.addAttribute("grade", index == Constants.NOT_FOUND ? new Grade() : grades.get(index));
        return "form";

    }

    @PostMapping("/handleSubmit")
    public String submitForm(Grade grade) {
        int index = getGradeIndex(grade.getId());
        if (index == Constants.NOT_FOUND){
            grades.add(grade);
        } else {
            grades.set(index, grade);
        }
        return "redirect:/grades";
    }

    public Integer getGradeIndex(String id){
        for(int i = 0; i < grades.size(); i++) {
            if(grades.get(i).getId().equals(id)) return i;
        }
        return Constants.NOT_FOUND;
    }

    @GetMapping("/completed")
    public String deleteGrade(String id) {
        for(int i=0; i < grades.size(); i++) {
            if(grades.get(i).getId().equals(id)){
                grades.remove(i);
            }
        }
        return "redirect:/grades";
    }

}
